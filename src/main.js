import Vue from 'vue'
import store from './store'
import router from './router'
import './style/reset.css'

// fullage.js
import 'fullpage.js/vendors/scrolloverflow'
import VueFullPage from 'vue-fullpage.js'
// BootstrapVue
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Ant-Design-Vue
import { Rate, Comment, Avatar } from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css'

// VueHighlightJS
import VueHighlightJS from 'vue-highlight.js';
import swift from 'highlight.js/lib/languages/swift';
import './style/xcode.css'; // custom xcode theme

import VueTextGlitch from 'vue-text-glitch'
import VueParticles from 'vue-particles'
import 'axios'
import App from './App.vue'

Vue.use(VueFullPage)
Vue.use(BootstrapVue)
Vue.use(Rate)
Vue.use(Comment)
Vue.use(Avatar)
Vue.use(VueHighlightJS, {
	languages: {
		swift
	}
})
Vue.use(VueTextGlitch)
Vue.use(VueParticles)
Vue.use(Rate)


Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: function (h) { return h(App) }
}).$mount('#app')
